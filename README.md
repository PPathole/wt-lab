# WT-Lab
All the codes of Web technology lab for _SPPU_

## HTML *and* CSS
1. Image Gallery
2. Student Registration form

## AJAX
1. Retrieval of XML data using AJAX and HTML

## Javascript
1. Student marks and percentage calculation
2. Simple form validation

## jQuery
1. Basic Animation by using `animate()`, `fadeIn()`, *and* `slideUp()` methods
2. Calculator using jQuery

## PHP *and* MySQL
1. CRUD operations using PHP *and* MySQL database on *localhost* using Apache

## XML *and* CSS
1. Making a basic XML file and performing styling by using CSS

## AngularJS
1. Building a calculator using AngularJS

## JSP
1. Build a basic calculator using JSP and HTML in Netbeans or Eclipse IDE

## Servlet
1. *Yet another* calculator using Servlets in Netbeans 

## EnterpriseJavaBeans (EJB)
1. Find out `Square`, `Cube`, *and* `Square root` of a number using EJB and Netbeans IDE
